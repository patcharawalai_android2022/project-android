package com.patcharawalai.bmrapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.patcharawalai.bmrapplication.databinding.FragmentBMRMaleBinding

class BMRMaleFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private var _binding : FragmentBMRMaleBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBMRMaleBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.calculateButton?.setOnClickListener {
            var weight = binding?.Weight?.text.toString().toDouble()
            var high = binding?.High?.text.toString().toDouble()
            var age = binding?.Age?.text.toString().toDouble()

            var sumweight = 66 + (13.7 * weight)
            var sumhigh = 5 * high
            var sumage = 6.8 * age
            var bmr = (sumweight + sumhigh) - sumage
            binding?.bmrResult?.text = "ค่า BMR: ${bmr}"

        }
        binding?.gohomeButton?.setOnClickListener {
            val action = BMRMaleFragmentDirections.actionBMRMaleFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}