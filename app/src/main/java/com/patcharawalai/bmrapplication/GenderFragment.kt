package com.patcharawalai.bmrapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.patcharawalai.bmrapplication.databinding.FragmentGenderBinding

class GenderFragment : Fragment() {
    private var _binding : FragmentGenderBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentGenderBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.maleState?.setOnClickListener { //ส่วนนี้คือการที่ทำให้กดปุ่มแล้วเปลี่ยนหน้าได้
            val action = GenderFragmentDirections.actionGenderFragmentToBMRMaleFragment()
            view.findNavController().navigate(action)
        }
        binding?.femaleState?.setOnClickListener {
            val action = GenderFragmentDirections.actionGenderFragmentToBMRFemaleFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}