package com.patcharawalai.bmrapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.patcharawalai.bmrapplication.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding

    // การกำหนดโครงสร้างหลัก จังหวะที่ Fragment เกิดขึ้นมา
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    //  จังหวะเชื่อมกับ layout
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    // เริ่มเอาตัวแปลต่างๆมาใช้
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.BMRButton?.setOnClickListener { //ส่วนนี้คือการที่ทำให้กดปุ่มแล้วเปลี่ยนหน้าได้
            val action = HomeFragmentDirections.actionHomeFragmentToGenderFragment()
            view.findNavController().navigate(action)
        }
    }

    // จังหวะที่เราต้องการจะลบ
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}